package student;

import java.util.*;

enum Polarity {
	POS, NEG, NEUT, NONE;
}

enum Strength {
	STRONG, WEAK, NONE;
}

public class Tweet {

	// TODO: Add appropriate data types
	Polarity gp;
	String id;
	String date;
	String user;
	String raw;
	String[] tokens;
	Polarity pp;
	Vector<String> neighbours = new Vector<>();
	Boolean visited = false;
 	
	
	
	
	
	
	
	public Tweet(String p, String i, String d, String u, String t) {
		// TODO
		switch (Integer.parseInt(p)) {
		case 0: this.gp = Polarity.NEG; break;
		case 2: this.gp = Polarity.NEUT; break;
		case 4: this.gp = Polarity.POS; break;
		default: this.gp = Polarity.NONE; break;
		}

		id = i;
		date = d;
		user =  u;
		raw = t;
		
		tokens = getWords();
		
	        
	 }
		
		
	
	
	public void addNeighbour(String ID) {
		// PRE: -
		// POST: Adds a neighbour to the current tweet as part of graph structure
		
		// TODO
		if (!id.equals(ID)) {
		neighbours.add(ID);
		}
		
	}
	
	public Integer numNeighbours() {
		// PRE: -
		// POST: Returns the number of neighbours of this tweet
		
		// TODO
		neighbours.trimToSize();
		return neighbours.size();
	}
	
	public void deleteAllNeighbours() {
		// PRE: -
		// POST: Deletes all neighbours of this tweet
		
		// TODO
		neighbours.clear();
		
	}
	
	public Vector<String> getNeighbourTweetIDs () {
		// PRE: -
		// POST: Returns IDs of neighbouring tweets as vector of strings
		
		// TODO
		
		
		return neighbours;
	}

	public Boolean isNeighbour(String ID) {
		// PRE: -
		// POST: Returns true if ID is neighbour of the current tweet, false otherwise
		
		// TODO
		if (neighbours.contains(ID)) return true;
		return false;
	}
	
	
	public Boolean correctlyPredictedPolarity () {
		// PRE: -
		// POST: Returns true if predicted polarity matches gold, false otherwise
		
		// TODO
		if (pp.equals(gp)) return true;

		return false;
	}
	
	public Polarity getGoldPolarity() {
		// PRE: -
		// POST: Returns the gold polarity of the tweet
		
		// TODO
		
		
		return gp;
	}
	
	public Polarity getPredictedPolarity() {
		// PRE: -
		// POST: Returns the predicted polarity of the tweet
		
		// TODO
		
		return pp;
	}
	
	public void setPredictedPolarity(Polarity p) {
		// PRE: -
		// POST: Sets the predicted polarity of the tweet

		// TODO
		this.pp = p;
		
		
		
	}
	
	public String getID() {
		// PRE: -
		// POST: Returns ID of tweet
		return id;
		// TODO

		
	}
	
	public String getDate() {
		// PRE: -
		// POST: Returns date of tweet
		
		// TODO

		return date;
	}
	
	public String getUser() {
		// PRE: -
		// POST: Returns identity of tweeter
		
		// TODO

		return user;
	}
	
	public String getText() {
		// PRE: -
		// POST: Returns text of tweet as a single string
		
		// TODO

		return raw;
	}
	
	public String[] getWords() {
		// PRE: -
		// POST: Returns tokenised text of tweet as array of strings
				
		if (this.getText() == null)
			return null;

		String[] words = null;
		
		String tmod = this.getText();
		tmod = tmod.replaceAll("@.*?\\s", "");
		tmod = tmod.replaceAll("http.*?\\s", "");
		tmod = tmod.replaceAll("\\s+", " ");
		tmod = tmod.replaceAll("[\\W&&[^\\s]]+", "");
		tmod = tmod.toLowerCase();
		tmod = tmod.trim();
		words = tmod.split("\\s");
		
		return words;
		
	}
	
}
