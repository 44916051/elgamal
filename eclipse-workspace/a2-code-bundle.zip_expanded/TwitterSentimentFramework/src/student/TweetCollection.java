package student;


import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.*;

import org.apache.commons.csv.*;
import org.junit.Test;


public class TweetCollection {

	// TODO: add appropriate data types
	LinkedList<Tweet> tweets;
	ArrayList<String> basicSent = new ArrayList<String>();
	int predictions = 0;
	Map<Integer, Vector<String>> connections = new TreeMap<>();
	Vector<String> currComp;

	public TweetCollection() {

		tweets = new LinkedList<Tweet>();
		// Constructor

		// TODO
	}

	/*
	 * functions for accessing individual tweets
	 */

	public Tweet getTweetByID (String ID) {
		// PRE: -
		// POST: Returns the Tweet object that with tweet ID

		for (int i = 0; i < tweets.size(); i++) {
			if (tweets.get(i).getID().equals(ID)) {
				return tweets.get(i);
			}
		}

		// TODO

		return null;
	}

	public Integer numTweets() {
		// PRE: -
		// POST: Returns the number of tweets in this collection

		// TODO
		return tweets.size();
	}


	/*
	 * functions for accessing sentiment words
	 */

	public Polarity getBasicSentimentWordPolarity(String w) {
		// PRE: w not null, basic sentiment words already read in from file
		// POST: Returns polarity of w

		// TODO
		if (isBasicSentWord(w)) {
			if (basicSent.contains(w + " negative")) {
				return Polarity.NEG;
			} else {
				return Polarity.POS;
			}
		}
		return Polarity.NONE;

	}

	public Polarity getFinegrainedSentimentWordPolarity(String w) {
		// PRE: w not null, finegrained sentiment words already read in from file
		// POST: Returns polarity of w

		// TODO

		return null;
	}

	public Strength getFinegrainedSentimentWordStrength(String w) {
		// PRE: w not null, finegrained sentiment words already read in from file
		// POST: Returns strength of w

		// TODO

		return null; 
	}

	/*
	 * functions for reading in tweets
	 * 
	 */


	public void ingestTweetsFromFile(String fInName) throws IOException {
		// PRE: -
		// POST: Reads tweets from .csv file, stores in data structure

		// NOTES
		// Data source, file format description at http://help.sentiment140.com/for-students
		// Using apache csv reader: https://www.callicoder.com/java-read-write-csv-file-apache-commons-csv/
		//call constructor for cllection


		try (
				Reader reader = Files.newBufferedReader(Paths.get(fInName));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
				) {

			Iterable<CSVRecord> csvRecords = csvParser.getRecords();

			for (CSVRecord csvRecord : csvRecords) {
				// Accessing Values by Column Index

				Tweet tw = new Tweet(csvRecord.get(0), // gold polarity
						csvRecord.get(1), 				// ID
						csvRecord.get(2), 				// date
						csvRecord.get(4), 				// user
						csvRecord.get(5));				// text

				// TODO: insert tweet tw into appropriate data type
				this.tweets.add(tw);

			}
		
		this.predictTweetSentimentFromBasicWordlist();
		}        
	}

	/*
	 * functions for sentiment words 
	 */

	public void importBasicSentimentWordsFromFile (String fInName) throws IOException {
		// PRE: -
		// POST: Read in and store basic sentiment words in appropriate data type

		// TODO

		BufferedReader br = new BufferedReader(new FileReader(fInName));
		try {
			String line = br.readLine();

			while(line !=null) {
				basicSent.add(line);
				line = br.readLine();	
			}
			
		} finally {
	
			br.close(); }
	
	}

	public void importFinegrainedSentimentWordsFromFile (String fInName) throws IOException {
		// PRE: -
		// POST: Read in and store finegrained sentiment words in appropriate data type

		// TODO
		
		BufferedReader br = new BufferedReader(new FileReader(fInName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while(line !=null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			String everything = sb.toString();
			System.out.print(everything + "1");
		} finally { br.close(); }
		
		  
	}


	public Boolean isBasicSentWord (String w) {
		// PRE: Basic sentiment words have been read in and stored
		// POST: Returns true if w is a basic sentiment word, false otherwise

		// TODO
		if (basicSent.contains(w + " negative") || basicSent.contains(w + " positive")) return true;
		else return false;


	}

	public Boolean isFinegrainedSentWord (String w) {
		// PRE: Finegrained sentiment words have been read in and stored
		// POST: Returns true if w is a finegrained sentiment word, false otherwise

		// TODO

		return null;
	}

	public void predictTweetSentimentFromBasicWordlist () {
		// PRE: Finegrained word sentiment already imported
		// POST: For all tweets in collection, tweet annotated with predicted sentiment
		//         based on count of sentiment words in sentWords
		// TODO

	
		
		for (int i = 0; i < tweets.size(); i++) {
			int pCount = 0;
			int nCount = 0;
				
			for (int j = 0; j < tweets.get(i).tokens.length;j++) {
			
				
		
					String curr = tweets.get(i).tokens[j];
					
			
					if (isBasicSentWord(curr)) {
					
						if (getBasicSentimentWordPolarity(curr) == Polarity.POS){
							
							pCount++;
						}
						if (getBasicSentimentWordPolarity(curr) == Polarity.NEG) {
							nCount++;
						
						}
						

				}
			}

			if (pCount + nCount == 0) {
				tweets.get(i).setPredictedPolarity(Polarity.NONE);
			} else if (pCount == nCount) {
				tweets.get(i).setPredictedPolarity(Polarity.NEUT);
				predictions++;
			} else if (pCount > nCount) {
				tweets.get(i).setPredictedPolarity(Polarity.POS);	
				predictions++;
			} else if (nCount > pCount) {
				tweets.get(i).setPredictedPolarity(Polarity.NEG);	
				predictions++;
				}

		}



	}

	public void predictTweetSentimentFromFinegrainedWordlist (Integer strongWeight, Integer weakWeight) {
		// PRE: Finegrained word sentiment already imported
		// POST: For all tweets in v, tweet annotated with predicted sentiment
		//         based on count of sentiment words in sentWords

		// TODO
	}

	/*
	 * functions for inverse index
	 * 
	 */

	public Map<String, Vector<String>> importInverseIndexFromFile (String fInName) throws IOException {
		// PRE: -
		// POST: Read in and returned contents of file as inverse index
		//         invIndex has words w as key, IDs of tweets that contain w as value

		// TODO
		
		Map<String,Vector<String>> invInd = new TreeMap<>();
		String key;
		
		BufferedReader br = new BufferedReader(new FileReader(fInName));
		try {
			String line = br.readLine();
			String temp[];
			
			
			while(line !=null) {
				Vector<String> val = new Vector<String>();
	
				temp = line.split("\\s");
				key = temp[0];
				

				temp = temp[1].split(",");
				val.addAll(Arrays.asList(temp));
				
				invInd.put(key,val);
			
				line = br.readLine();
			}
			
	
		} finally {
			br.close(); }
	

		return invInd;
		
	}


	/*
	 * functions for graph construction 
	 */

	public void constructSharedWordGraph(Map<String, Vector<String>> invIndex) {
		// PRE: invIndex has words w as key, IDs of tweets that contain w as value
		// POST: Graph constructed, with tweets as vertices, 
		//         and edges between them if they share a word

		// TODO
		for (Map.Entry<String, Vector<String>> entry : invIndex.entrySet() ) {
			Vector<String> curr = entry.getValue();
			if (curr.size() > 1) {
				for (int i = 0; i < curr.size() ; i++) {
					for (int j =0; j < curr.size(); j++) {
						if (getTweetByID(curr.get(i)) != null && getTweetByID(curr.get(j)) != null) {
					getTweetByID(curr.get(i)).addNeighbour(curr.get(j));
						}
					}
				}
			}
		}
		
	}

	public Integer numConnectedComponents() {
		// PRE: -
		// POST: Returns the number of connected components

		// TODO
		
		return connections.size();
	}

	public void annotateConnectedComponents() {
		// PRE: -
		// POST: Annotates graph so that it is partitioned into components
		for (int i = 0; i < tweets.size(); i++) {
			currComp = new Vector<>();
			if (!tweets.get(i).visited) {
			dft(tweets.get(i).getID(), i);
			}
			if (!currComp.isEmpty()) {
				connections.putIfAbsent(i, currComp);
			}
		}
		
	}
	public void dft (String ID, int i) {
		
		
			
			getTweetByID(ID).visited = true;
			if (!currComp.contains(ID)) {
			currComp.add(ID);
			}
			Vector<String> n = getTweetByID(ID).getNeighbourTweetIDs();
		
			Iterator<String> iter = n.iterator();
			
			while (iter.hasNext()) {
				String next = iter.next();
				if (!getTweetByID(next).visited) {
					dft(next, i);
			}
		}
		
	}



	public Integer componentSentLabelCount(String ID, Polarity p) {
		// PRE: Graph components are identified, ID is a valid tweet
		// POST: Returns count of labels corresponding to Polarity p in component containing ID

		// TODO
		int count = 0;
		for (Map.Entry<Integer, Vector<String>> entry : connections.entrySet()) {

			Vector<String> curr = entry.getValue();
			System.out.println(curr);
	
			if (curr.contains(ID)) {
				for (int i = 0; i < curr.size(); i++) {
					if (getTweetByID(curr.get(i)).getPredictedPolarity() == p) {
						count++;
					}
				}
				return count;
			}
		}
		return null;
	}


	public void propagateLabelAcrossComponent(String ID, Polarity p, Boolean keepPred) {
		// PRE: ID is a tweet id in the graph
		// POST: Labels tweets in component with predicted polarity p 
		//         (if keepPred == T, only tweets w pred polarity None; otherwise all tweets

		// TODO
		
	
			

		for (Map.Entry<Integer, Vector<String>> entry : connections.entrySet()) {
			
			Vector<String> curr = entry.getValue();
			System.out.println(curr);
			if (curr.contains(ID)) {
				for (int i = 0 ; i < curr.size(); i++) {
					if (keepPred) {
						 if (getTweetByID(curr.get(i)).getPredictedPolarity() == Polarity.NONE) {
							 getTweetByID(curr.get(i)).setPredictedPolarity(p);
						 }
					} else {
						getTweetByID(curr.get(i)).setPredictedPolarity(p);
					}
				}
			}
			
		}


	} 


	public void propagateMajorityLabelAcrossComponents(Boolean keepPred) {
		// PRE: Components are identified
		// POST: Tweets in each component are labelled with the majority sentiment for that component
		//       Majority label is defined as whichever of POS or NEG has the larger count;
		//         if POS and NEG are both zero, majority label is NONE
		//         otherwise, majority label is NEUT
		//       If keepPred is True, only tweets with predicted label None are labelled in this way
		//         otherwise, all tweets in the component are labelled in this way

		// TODO
		for (Map.Entry<Integer, Vector<String>> entry : connections.entrySet()) {

			Vector<String> curr = entry.getValue();
			System.out.println(curr);
			Polarity p;
			
			for (int i = 0 ; i < curr.size(); i++) {
				int pCount = 0;
				int nCount = 0;
				
				for (int j = 0; j <curr.size(); j++) {
					if (getTweetByID(curr.get(j)).getPredictedPolarity() == Polarity.POS) {
						pCount++;
					}
					if (getTweetByID(curr.get(j)).getPredictedPolarity() == Polarity.NEG) {
						nCount++;
					}
				}
				if (pCount + nCount == 0) {
					p = Polarity.NONE;
				} else if (pCount > nCount) {
					p = Polarity.POS;
				} else if (nCount > pCount) {
					p = Polarity.NEG;
				} else {
					p = Polarity.NEUT;
				}
				
				if (keepPred) {
					if (getTweetByID(curr.get(i)).getPredictedPolarity() == Polarity.NONE) {
						getTweetByID(curr.get(i)).setPredictedPolarity(p);
					}
				} else {
					getTweetByID(curr.get(i)).setPredictedPolarity(p);
				}
			}
		}


		
	}



	/*
	 * functions for evaluation 
	 */

	public Double accuracy () {
		// PRE: -
		// POST: Calculates and returns accuracy of labelling

		// TODO
		double count = 0;
		double attempt = 0;

		for(int i = 0; i < tweets.size(); i++) {
			if (tweets.get(i).getPredictedPolarity() != Polarity.NONE) {
				attempt++;
				if (tweets.get(i).getPredictedPolarity() == tweets.get(i).getGoldPolarity()) {
					count++;
				}
			}
		}

		return count/attempt;
	}

	public Double coverage () {
		// PRE: -
		// POST: Calculates and returns coverage of labelling

		// TODO
		
		
		return predictions/((double)(tweets.size()));
	}



	public static void main(String[] args) {


	}

}
